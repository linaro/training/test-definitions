pytest with LAVA instrumentation
================================

This directory contains a complete example showing how to integrate
pytest with LAVA.

It consists of:

* `lava_plugin.py` - A simple pytest plugin that emits LAVA signals for
  each test case
* `pytest-example.yaml` - LAVA test definition used to run the test
* `test_example.py` - A tiny, tiny test suite
