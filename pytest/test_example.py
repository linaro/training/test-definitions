#
# Trivial test suite with one passing and one failing test
#

def inc(x):
    return x + 1

def test_good_answer():
    assert inc(3) == 4

def test_bad_answer():
    assert inc(3) == 5
