#
# Simple pytest plugin to emit LAVA signals for each test case
#

def lava_result_convert(pytest_outcome):
    return 'pass' if pytest_outcome == 'passed' else 'fail'

# When a test fails pytest will call the teststatus hook for a second time
# during the failure reporting summary. We will using this boolean to
# suppress results that aren't emitted between 'setup' and 'teardown'.
running = True

def pytest_report_teststatus(report):
    global running

    test_name = report.location[2][5:]
    if report.when == 'setup':
        running = True
        print(f'\n<LAVA_SIGNAL_STARTTC {test_name}>')
    elif report.when == 'call' and running:
        test_result = lava_result_convert(report.outcome)
        print(f'\n<LAVA_SIGNAL_ENDTC {test_name}>')
        print(f'<LAVA_SIGNAL_TESTCASE TEST_CASE_ID={test_name} RESULT={test_result}>')
    elif report.when == 'teardown':
        running = False
