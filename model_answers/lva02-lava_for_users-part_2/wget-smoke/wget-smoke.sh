#!/bin/sh

wget_test () {
	f=$(echo $1 | cut -f3 -d/)
	lava-test-case fetch-from-$f --shell wget -O $f.html $1
	lava-test-case html-check-$f --shell grep -q html $f.html
}


timed_test () {
	f=$(echo $1 | cut -f3 -d/)
	time -o timings echo "Replace this echo with test command"
	if [ $? -eq 0 ]
	then
		elapsed=$(sed -e 's/m//' -e 's/s//' timings | awk '/real/ {print $2*60+$3}')
		lava-test-case benchmark-$f --result pass --measurement $elapsed --units seconds
	else
		lava-test-case benchmark-$f --result fail

	fi
}

wget_test http://fileserver
wget_test https://google.com
wget_test http://does.not.exist

timed_test http://fileserver
timed_test https://google.com
timed_test http://does.not.exist
